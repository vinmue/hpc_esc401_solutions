#include <stdio.h>			
#include <omp.h>
#include <stdlib.h>
int main(int argc, char* argv[]){
      
        long prime = strtol(argv[1], NULL, 10);
        int i;
    #pragma omp parallel for reduction
	for (i = 2 ; (i < prime) & (prime % i !=0) ; i += 1) {}	
    
        if ( i == prime ){
		printf("%u is a prime\n", prime);
	    }
        else{
		printf("%u is not a prime\n", prime);
	    }
    
        return 0;
}

