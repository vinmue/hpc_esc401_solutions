#!/bin/bash
echo '1) end with 00'
egrep --color 00$ $1
echo '2) start and end with 1'
egrep --color '^1.*1$'  $1 
echo '3) contain the pattern 110'
egrep --color 110 $1 
echo '4) contain at least three times a 1'
egrep --color '(.*1){3,}' $1
echo '5) contain at least three consecutive 1s'
egrep --color 111 $1 

