#!/bin/bash -l
#SBATCH --job-name="job_omp"
#SBATCH --mail-type=ALL
#SBATCH --mail-user=vincent.mueller@uzh.ch
#SBATCH --time=00:20:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-core=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=36
#SBATCH --partition=debug
#SBATCH --constraint=mc
#SBATCH --hint=nomultithread
#SBATCH --account=uzh8
#SBATCH --cpus-per-task=36


if ! test -n "$1" ; then echo "you don't input number, type it now:";
while read input; do

if [[ "$input" =~ ^[0-9]+$ ]] ; then break; fi
echo "your input must be apositive integer" ;
done

elif ! [[ "$1" =~ ^[0-9]+$ ]] ;
then echo "your input must be a positive integer";
while read input; do

if [[ "$input" =~ ^[0-9]+$ ]] ; then break; fi
echo "your input must be a positive integer" ;
done
else input="$1"
fi
echo "$input"


export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

time srun prime $input

