#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
int main(int argc, char *argv[]){
	long lower = strtol(argv[1], NULL, 10 );
	long upper = strtol(argv[2], NULL, 10 );
	long i;
	long prime;
	printf("this are the prime from %u to %u\n",lower, upper );
    #pragma omp parallel for reduction
	for (prime=lower; prime < upper+1; prime++ ){
		for (i=2; (i < prime) & (prime % i !=0 ); i++){
	    #pragma omp for reduction
		}
		if (i==prime){printf("%u ",prime); }
	}
 printf("\n");
 return 0;
}
