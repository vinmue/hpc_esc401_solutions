#!/bin/bash -l
#SBATCH --job-name="serial_job"																			    
#SBATCH --mail-type=ALL
#SBATCH --mail-user=vincent.mueller@uzh.ch
#SBATCH --time=00:30:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-core=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=debug
#SBATCH --constraint=mc
#SBATCH --hint=nomultithread
#SBATCH --account=uzh8
#SBATCH -o cpi_output.log																																								        
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

srun cpi
