#!/usr/bin/env python
import os
import sys

input1 =int( sys.argv[1])
input2 =int( sys.argv[2])



scratch = os.environ['SCRATCH']

def mkdir_p(dir):
    if not os.path.exists(dir):
        os.mkdir(dir)




for num in range(input1,input2,1):

    
    job_file = os.path.join(scratch,"sum%s.sh"%num)
        

    
    new_file= open(job_file,"w")
    new_file.close()
    with open(job_file,"w") as fh:
	fh.writelines("#!/bin/bash -l\n")
	fh.writelines("#SBATCH --job-name=%ssum.job\n"%num)
	fh.writelines("#SBATCH --mail-type=ALL\n")
	fh.writelines("#SBATCH --mail-user=vincent.mueller@uzh.ch\n")
	fh.writelines("#SBATCH --time=00:05:00\n")
	fh.writelines("#SBATCH --nodes=1\n")
	fh.writelines("#SBATCH --output=sum%s.out\n" %num)
	fh.writelines("#SBATCH --ntasks-per-core=1\n")
	fh.writelines("#SBATCH --ntasks-per-node=1\n")
	fh.writelines("#SBATCH --cpus-per-task=%s\n"%num)
	fh.writelines("#SBATCH --partition=debug\n")
	fh.writelines("#SBATCH --constraint=mc\n")
	fh.writelines("#SBATCH --hint=nomultithread\n")
	fh.writelines("#SBATCH --account=uzh8\n")
	fh.writelines("export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK\n")

	fh.writelines("srun sum_simple")
    os.system("sbatch %s" %job_file)
